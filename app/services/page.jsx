"use client";
import React, { useState } from "react";
import Head from "next/head";
import OurProcess from "@components/ServicePage/OurProcess";
import StartNewProject from "@components/ServicePage/StartNewProject";
import OurQualityService from "@components/ServicePage/OurQualityService";
import TaglineHeaderComponent from "@components/HomePage/TaglineHeaderComponent";
import serviceBanner from "../../public/assets/images/ServicesPage/PerfectServiceBanner.webp";
import { serviceData } from "@utils/ConstantPageData/Services";
import HowWeBuildSoftware from "@components/ServicePage/HowWeBuildSoftware";
import BottomToTopComponent from "@components/ButtonComponent/BottomToTopComponent";
import { useRouter } from "next/navigation";

const metadata = {
  title: "My Next.js App",
  description: "This is my Next.js app.",
};

const Services = () => {
  const router = useRouter();
  const [activeOurProcess, setActiveOurProcess] = useState(0);

  const onClickChangeProcess = (data) => () => {
    setActiveOurProcess(data.id);
  };

  return (
    <React.Fragment>
      <Head>
        <title>{metadata.title}</title>
        <meta name="description" content={metadata.description} />
      </Head>
      <BottomToTopComponent />
      <TaglineHeaderComponent
        title={serviceData.SectionOneHead}
        desc={serviceData.SectionOneDesc}
        onClickAction={() => router.push("/contactus")}
        imgSrc={serviceBanner}
      />
      <OurQualityService />
      <OurProcess
        activeCount={activeOurProcess}
        onClickAction={onClickChangeProcess}
      />
      <HowWeBuildSoftware />
      <StartNewProject onClickAction={() => router.push("/contactus")} />
    </React.Fragment>
  );
};

export default Services;
