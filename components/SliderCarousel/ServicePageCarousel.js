import React, { useRef, useState } from "react";
import PropTypes from "prop-types";
import Slider from "@ant-design/react-slick";
import { ServiceSectionFourHeadArr } from "@utils/ConstantPageData/Services";
import Image from "next/image";
import planningImg from "../../public/assets/icons/planning.svg";
import designImg from "../../public/assets/icons/design.svg";
import developmentImg from "../../public/assets/icons/development.svg";
import activeLeft from "../../public/assets/icons/Arrow-right-Disable.svg";
import activeRight from "../../public/assets/icons/Arrow-right-active.svg";

const ServicePageCarousel = () => {
  const [oldSlide, setOldSlide] = useState(0);
  let sliderRef = useRef(null);
  const next = () => {
    sliderRef.slickNext();
  };
  const previous = () => {
    sliderRef.slickPrev();
  };

  const settings = {
    dots: false,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 500,
    arrows: false,
    centerMode: true,
    centerPadding: "60px",
    slidesToShow: 2,
    beforeChange: (current, next) => {
      setOldSlide(current);
    },
    afterChange: (current, prev) => {
      setOldSlide(current);
    },
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          // centerPadding: "40px",
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <div className="pb-12 main-services-slider">
      <Slider
        ref={(slider) => {
          sliderRef = slider;
        }}
        {...settings}
      >
        {ServiceSectionFourHeadArr.map((data) => {
          return (
            <React.Fragment key={data.id}>
              <ReuseCardComponent
                dataInfo={data}
                currentSlide={oldSlide}
                imgSrc={
                  data.id === 0
                    ? planningImg
                    : data.id === 1
                    ? designImg
                    : developmentImg
                }
                title={data.title}
                desc={data.description}
              />
            </React.Fragment>
          );
        })}
      </Slider>
      <div className="text-end my-10 mr-24">
        <button className="button mr-9" onClick={previous}>
          <Image
            src={activeLeft}
            layout="responsive"
            alt="Left Arrow"
            title="Left Arrow"
          />
        </button>
        <button className="button" onClick={next}>
          <Image
            src={activeRight}
            layout="responsive"
            alt="Right Arrow"
            title="Right Arrow"
          />
        </button>
      </div>
    </div>
  );
};

const ReuseCardComponent = ({
  dataInfo,
  currentSlide,
  imgSrc,
  title,
  desc,
}) => {
  return (
    <div
      className={`card-box-style`}
      style={{
        backgroundColor: `${
          dataInfo.id === currentSlide ? "#8A66C5" : "#F0F0F0"
        }`,
        border: `1px solid ${
          dataInfo.id === currentSlide ? "#8A66C5" : "#F0F0F0"
        }`,
      }}
    >
      <Image
        src={imgSrc}
        className="w-20 h-20"
        loading="eager"
        // layout="responsive"
        alt={title}
        title={title}
      />
      <h4 className="typoStyles700_28 text-black my-3">{title}</h4>
      <h4
        className={`typoStyles400_30_20 ${
          dataInfo.id === currentSlide
            ? "text-white"
            : "text-light-gray-pann-two"
        }`}
      >
        {desc}
      </h4>
    </div>
  );
};

ReuseCardComponent.propTypes = {
  dataInfo: PropTypes.array || PropTypes.object,
  currentSlide: PropTypes.number,
  imgSrc: PropTypes.string || PropTypes.object,
  title: PropTypes.string,
  desc: PropTypes.string,
};

export default ServicePageCarousel;
