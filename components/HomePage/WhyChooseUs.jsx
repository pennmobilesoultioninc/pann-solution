import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "antd";
import Image from "next/image";
import { SectionFourHeadArr, homeData } from "@utils/ConstantPageData/Home";
import whyChooseVector from "../../public/assets/images/HomePage/whyChooseVector.svg";
import QualityAssurance from "../../public/assets/icons/QualityAssurance.svg";
import CostEfficiency from "../../public/assets/icons/Costefficiency.svg";
import Reliability from "../../public/assets/icons/Reliability.svg";

const WhyChooseUs = () => {
  return (
    <Row
      justify={"center"}
      className="bg-primary-violet py-16 md:py-24 px-3 md:px-4 lg:px-6 xl:px-24 relative"
    >
      <div className="home-spread-top-pink"></div>
      <Col xs={24} xl={20}>
        <h4 className="typoStyles500_60 mb-7 text-center text-white">
          {homeData.SectionFourHead}
        </h4>
        <div className="w-full mx-auto">
          <p className="typoStyles400_20 text-center text-light-gray-pann-two">
            {homeData.SectionFourDesc}
          </p>
        </div>
        <Row justify={"center"} className="mt-8 md:mt-24 relative">
          <Image
            src={whyChooseVector}
            // width={"100%"}
            // height={"100%"}
            // style={{ objectFit: "contain" }}
            className="why-choose-vector-img hidden md:hidden lg:hidden xl:block no-display-on-ipad"
            alt={homeData.SectionFourHead}
            title={homeData.SectionFourHead}
          />
          {SectionFourHeadArr.map((data) => {
            return (
              <React.Fragment key={data.id}>
                <ReuseCardComponent
                  iconImage={
                    data.id === 0
                      ? QualityAssurance
                      : data.id === 1
                      ? CostEfficiency
                      : Reliability
                  }
                  title={data.title}
                  desc={data.description}
                />
              </React.Fragment>
            );
          })}
        </Row>
      </Col>
      <div className="home-spread-bottom-pink"></div>
    </Row>
  );
};

const ReuseCardComponent = ({ title, desc, iconImage }) => {
  return (
    <Col
      xs={24}
      md={8}
      lg={8}
      xl={8}
      className="text-center px-12 md:px-4 lg:px-6 xl:px-14 mb-5 md:mb-0"
    >
      <div className="flex items-center justify-center mx-auto w-20 h-20 bg-white rounded-full mb-8 md:mb-14">
        <Image
          src={iconImage}
          width={"100%"}
          height={"100%"}
          alt={title}
          title={title}
        />
      </div>
      <h5 className="typoStyles500_20 text-white mb-4">{title}</h5>
      <p className="typoStyles400_16 text-light-text">{desc}</p>
    </Col>
  );
};

ReuseCardComponent.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  iconImage: PropTypes.string || PropTypes.object,
};

export default WhyChooseUs;
