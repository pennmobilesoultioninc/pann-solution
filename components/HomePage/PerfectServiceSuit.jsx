import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "antd";
import Image from "next/image";
import semiSphere from "../../public/assets/icons/Semisphere.svg";
import { homeData, SectionTwoHeadArr } from "@utils/ConstantPageData/Home";
import appleImg from "../../public/assets/icons/apple-fill.svg";
import codeImg from "../../public/assets/icons/code-fill.svg";
import androidImg from "../../public/assets/icons/android-fill.svg";
import consultImg from "../../public/assets/icons/consult-fill.svg";

const PerfectServiceSuit = () => {
  return (
    <div className="flex justify-center home-perfect-suit relative bg-primary-creme m-0">
      <Image
        src={semiSphere}
        width={110}
        height={110}
        className="absolute right-0 top-0 hidden md:block"
        alt={homeData.SectionTwoHead}
        title={homeData.SectionTwoHead}
      />
      <div className="w-full md:w-full lg:w-4/5 px-2 md:px-10 lg:px-0">
        <h4 className="typoStyles500_60 mb-7 text-center">
          {homeData.SectionTwoHead}
        </h4>
        <div className="w-full px-1 md:px-11 mx-auto">
          <p className="typoStyles400_20 text-center text-light-gray-pann-two">
            {homeData.SectionTwoDesc}
          </p>
        </div>
        <div className="mt-6 md:mt-14 px-3 md:px-0 justify-center relative">
          <div className="spreadBox-color"></div>
          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
            {SectionTwoHeadArr.map((data) => {
              return (
                <React.Fragment key={data.id}>
                  <ReuseCardComponent
                    type={data.type}
                    title={data.title}
                    desc={data.description}
                  />
                </React.Fragment>
              );
            })}
          </Row>
        </div>
      </div>
    </div>
  );
};

const ReuseCardComponent = ({ type, title, desc }) => {
  return (
    <Col xs={24} md={12} className={`flex`}>
      <div className="md:flex md:items-start md:justify-between py-7 md:py-12 px-5 mb-8 home-card-style hover:bg-light-blue-hover-pann text-center md:text-left">
        <div className="w-full md:w-1/4 flex justify-center">
          <Image
            src={
              type.toString() === "android"
                ? androidImg
                : type.toString() === "ios"
                ? appleImg
                : type.toString() === "software"
                ? codeImg
                : consultImg
            }
            // width={104}
            // height={104}
            className="w-20 md:w-24 h-20 md:h-24"
            style={{ objectFit: "contain" }}
            alt={title}
            title={title}
          />
        </div>
        <div className="w-full md:w-3/4">
          <h4 className="typoStyles700_28 mt-1 mb-4">{title}</h4>
          <div className="w-full md:pr-12">
            <p className="typoStyles400_16 text-light-gray-pann-two mb-4">
              {desc}
            </p>
          </div>
          {/* <div>
            <h3
              className="typoStylesOswald500_28 text-purple-two"
              style={{ textDecoration: "underline" }}
            >
              KNOW MORE
            </h3>
          </div> */}
        </div>
      </div>
    </Col>
  );
};

ReuseCardComponent.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
};

export default PerfectServiceSuit;
