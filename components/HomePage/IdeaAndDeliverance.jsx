"use client";
import { Col, Row } from "antd";
import PropTypes from "prop-types";
import { homeData } from "@utils/ConstantPageData/Home";
import ShadowButtonComp from "@components/ButtonComponent/ShadowButtonComp";
import ideaDeliverance from "../../public/assets/images/HomePage/ideadeliverance.webp";
import Image from "next/image";

const IdeaAndDeliverance = (props) => {
  const { onClickAction } = props;
  return (
    <Row
      justify={"center"}
      className="bg-primary-creme pt-10 pb-10 md:py-36 px-4 relative m-0"
    >
      <Col xs={24} md={16} lg={10} xl={10} className="text-center">
        <h1 className={"typoStyles500_40 mb-6"}>{homeData.SectionThreeHead}</h1>
        <p className={"typoStyles400_30_20"}>{homeData.SectionThreeDesc}</p>
        <div className="flex justify-center items-center mt-9 md:mt-14">
          <ShadowButtonComp
            btnTitle="Contact Us"
            borderColor={"#000000"}
            backgroundColor={"#000000"}
            onClickBtn={onClickAction}
          />
        </div>
      </Col>
      <Image
        src={ideaDeliverance}
        width={"100%"}
        height={"100%"}
        className="absolute bottom-0 right-0 hidden md:hidden lg:hidden xl:block no-display-on-ipad"
        style={{ objectFit: "contain" }}
        alt={homeData.SectionThreeHead}
        title={homeData.SectionThreeHead}
      />
    </Row>
  );
};

IdeaAndDeliverance.propTypes = {
  onClickAction: PropTypes.func,
};

export default IdeaAndDeliverance;
