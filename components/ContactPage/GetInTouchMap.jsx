import React from "react";
import { Col, Row } from "antd";

const GetInTouchMap = () => {
  return (
    <Row justify={"center"}>
      <Col xs={24} xl={24}>
        <iframe
          title="Pann Solutions"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3058.5320171514963!2d-75.17628262432625!3d39.9518564838025!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c6c63714e0dd71%3A0xe8b24809501f1f64!2s104%20S%2020th%20St%2C%20Philadelphia%2C%20PA%2019103%2C%20USA!5e0!3m2!1sen!2sin!4v1715969727524!5m2!1sen!2sin"
          className="w-full h-72 md:h-96"
          allowFullScreen={true}
          loading="eager"
          referrerPolicy="no-referrer-when-downgrade"
        />
      </Col>
    </Row>
  );
};

export default GetInTouchMap;
