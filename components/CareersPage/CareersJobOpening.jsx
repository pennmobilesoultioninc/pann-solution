import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import iconArrow from "../../public/assets/icons/Arrow-Down-Disable.svg";
import {
  faMinus,
  faLocationDot,
  faClock,
} from "@fortawesome/free-solid-svg-icons";
import {
  careerData,
  careerJobOpeningsData,
} from "@utils/ConstantPageData/Career";
import ShadowButtonComp from "@components/ButtonComponent/ShadowButtonComp";
import Image from "next/image";
import moment from "moment";

const CareersJobOpening = (props) => {
  const { onClickButtonModal, refId } = props;
  return (
    <Row
      ref={refId}
      className="bg-primary-violet px-5 md:px-12 lg:px-44 mainPaddingTopBottom100 relative"
    >
      <div className="career-job-spread-top-pink  hidden md:block"></div>
      <div className="career-job-spread-top-pink-two hidden md:block"></div>
      <Col span={24}>
        <span className="typoStyles500_16 text-white flex items-center align-middle">
          <FontAwesomeIcon
            icon={faMinus}
            className="fas faMinus mr-4"
            style={{ color: "#ffffff", fontSize: "1.525rem" }}
          />
          {careerData.SectionThreeHead}
        </span>
        <h4 className="typoStyles700_42 text-white pt-5">
          {careerData.SectionThreeHeadSub}
        </h4>
      </Col>
      <Col span={24} className="my-8 md:my-16">
        {careerJobOpeningsData.map((data) => {
          return (
            <React.Fragment key={data.id}>
              <ReuseJobOpeningCard
                jobData={data}
                onClickButton={onClickButtonModal}
              />
            </React.Fragment>
          );
        })}
      </Col>
      <Col span={24}>
        <div className="bg-transparent flex items-center justify-end border-none typoStyles700_16 text-white ">
          View more
          <Image
            src={iconArrow}
            className="w-11 h-11 md:w-24 md:h-24"
            alt="View More"
            title="View More"
          />
        </div>
      </Col>
    </Row>
  );
};

const ReuseJobOpeningCard = ({ jobData, onClickButton }) => {
  const jobPostDate = moment(jobData.created_at);
  const currentDate = moment().format("YYYY-MM-DD");
  const currentDateTwo = moment(currentDate);
  const totalDateCount = currentDateTwo.diff(jobPostDate, "days");
  const totalTime = moment(jobPostDate, currentDateTwo).fromNow();

  return (
    <div className="bg-light-blue-hover-pann w-full px-4 md:px-8 py-8 md:py-11">
      <div className="flex justify-between mb-4">
        <div className="md:flex items-start justify-start">
          <h4 className="typoStyles700_28 text-black md:mr-14">
            {jobData?.jobName}
          </h4>
          <div className="flex md:items-center mt-5 md:mt-0">
            {jobData?.jobTypeSpec.map((item) => {
              return (
                <React.Fragment key={item}>
                  <div className="flex items-center justify-center bg-purple-five px-4 py-1 md:py-2 mr-3 md:mr-5">
                    <p className="typoStyles500_14 text-black">{item}</p>
                  </div>
                </React.Fragment>
              );
            })}
          </div>
        </div>
        <div className="hidden md:block">
          <ShadowButtonComp
            btnTitle="Apply Now"
            borderColor={"#000000"}
            backgroundColor={"#000000"}
            onClickBtn={onClickButton}
          />
        </div>
      </div>
      <div className="w-full md:w-9/12">
        <p className="typoStyles400_30_18 text-black mb-3 md:mb-4">
          {jobData?.jobDesc}
        </p>
        <p className="typoStyles700_18 text-black">
          Key Responsibilities:{" "}
          <span className="typoStyles400_30_18">
            {jobData?.jobResponsibility}
          </span>
        </p>
        <br className="hidden md:block" />
        <p className="typoStyles700_18 text-black">
          Minimum years of Exp:{" "}
          <span className="typoStyles400_30_18">{jobData?.jobExperience}</span>
        </p>
        <br className="hidden md:block" />
        <p className="typoStyles700_18 text-black">
          Education:{" "}
          <span className="typoStyles400_30_18">{jobData?.jobEducation}</span>
        </p>
        <br className="hidden md:block" />
        <p className="typoStyles700_18 text-black">
          Technical Skills:{" "}
          <span className="typoStyles400_30_18">
            {jobData?.jobTechnicalSkills}
          </span>
        </p>
        <br className="hidden md:block" />
        <p className="typoStyles700_18 text-black">
          Tools:{" "}
          <span className="typoStyles400_30_18">{jobData?.jobTools}</span>
        </p>
        <br className="hidden md:block" />
        <p className="typoStyles700_18 text-black">
          Soft Skills:{" "}
          <span className="typoStyles400_30_18">{jobData?.jobSoftSkills}</span>
        </p>
        <br className="hidden md:block" />
        <p className="typoStyles700_18 text-black">
          Preferred Qualifications:{" "}
          <span className="typoStyles400_30_18">
            {jobData?.jobPreferredQualifications}
          </span>
        </p>
        <br className="hidden md:block" />
        <p className="typoStyles700_18 text-black">
          What We Offer:{" "}
          <span className="typoStyles400_30_18">{jobData?.JobWeOffer}</span>
        </p>
        <br className="hidden md:block" />
        <p className="typoStyles700_18 text-black">
          Shift timing:{" "}
          <span className="typoStyles400_30_18">{jobData?.jobShiftTiming}</span>
        </p>
      </div>
      <div className="w-full border-t border-gray-400 mt-4 md:mt-8 pt-3 md:pt-6">
        <div className="flex items-center">
          {jobData?.jobType !== undefined &&
            jobData?.jobType !== null &&
            jobData?.jobType.length > 0 && (
              <span className="flex items-center mr-6">
                <FontAwesomeIcon
                  icon={faLocationDot}
                  className="fas faLocationDot mr-2"
                  style={{ color: "#000000", fontSize: "1rem" }}
                />
                <span className="typoStyles400_28_16">{jobData?.jobType}</span>
              </span>
            )}
          <span className="flex items-center">
            <FontAwesomeIcon
              icon={faClock}
              className="fas faClock mr-2"
              style={{ color: "#000000", fontSize: "1rem" }}
            />
            <span className="typoStyles400_28_16">{totalTime}</span>
          </span>
        </div>
        <div className="w-full mt-6 block md:hidden">
          <ShadowButtonComp
            extraCss={"career-apply-btn"}
            btnTitle="Apply Now"
            borderColor={"#000000"}
            backgroundColor={"#000000"}
            onClickBtn={onClickButton}
          />
        </div>
      </div>
    </div>
  );
};

CareersJobOpening.propTypes = {
  onClickButtonModal: PropTypes.func,
  refId:
    PropTypes.number || PropTypes.string || PropTypes.object || PropTypes.func,
};

ReuseJobOpeningCard.propTypes = {
  jobData: PropTypes.object || PropTypes.string,
  onClickButton: PropTypes.func,
};

export default CareersJobOpening;
