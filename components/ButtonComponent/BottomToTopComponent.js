import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretUp } from "@fortawesome/free-solid-svg-icons";

const BottomToTopComponent = () => {
  const [isScrolled, setIsScrolled] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      setIsScrolled(window.scrollY > 0);
    });
  }, []);

  return (
    <div className="bottom-to-top">
      {isScrolled && (
        <button onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}>
          <FontAwesomeIcon
            icon={faCaretUp}
            className="fas faCaretUp"
            style={{
              fontSize: "2rem",
            }}
          />
        </button>
      )}
    </div>
  );
};

export default BottomToTopComponent;
