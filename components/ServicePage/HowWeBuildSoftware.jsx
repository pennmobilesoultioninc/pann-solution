import ServicePageCarousel from "@components/SliderCarousel/ServicePageCarousel";
import ServicePageMobileCarousel from "@components/SliderCarousel/ServicePageMobileCarousel";
import { serviceData } from "@utils/ConstantPageData/Services";
import { Col, Row } from "antd";
import React from "react";

const HowWeBuildSoftware = () => {
  return (
    <div className="bg-primary-creme relative">
      <div className="flex justify-center service-our-quality pb-9">
        <div className="w-full md:w-4/5">
          <h4 className="typoStyles500_60 mb-4 md:mb-10 text-center">
            {serviceData.SectionFourHead}
          </h4>
          <div className="w-full px-4 md:px-0 md:w-4/5 mb-4 mx-auto">
            <p className="typoStyles400_20 text-center text-light-gray-pann-two">
              {serviceData.SectionFourDesc}
            </p>
          </div>
        </div>
      </div>
      <Row justify={"center"}>
        <Col xs={24} xl={24} className="hidden md:block pl-10">
          <ServicePageCarousel />
        </Col>
        <Col xs={24} xl={24} className="block md:hidden">
          <ServicePageMobileCarousel />
        </Col>
      </Row>
    </div>
  );
};

export default HowWeBuildSoftware;
