import { Space_Grotesk, Poppins, Oswald } from "next/font/google";
import localFont from "next/font/local";

export const spaceGrotesk = Space_Grotesk({
  subsets: ["latin"],
  weight: ["300", "400", "500", "600", "700"],
  variable: "--font-space-grotesk",
});

export const poppins = Poppins({
  subsets: ["latin"],
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
  variable: "--font-poppins",
});

export const oswald = Oswald({
  subsets: ["latin"],
  weight: ["200", "300", "400", "500", "600", "700"],
  variable: "--font-oswald",
});

export const sportingGrotesque = localFont({
  src: [
    {
      path: "../public/fonts/SportingGrotesque-Regular.otf",
      weight: "400",
    },
    {
      path: "../public/fonts/SportingGrotesque-Bold.otf",
      weight: "700",
    },
  ],
  variable: "--font-sporting-Grotesque",
});
