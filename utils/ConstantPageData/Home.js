const homeData = {
  SectionOneHead: "Apps For All Screens",
  SectionOneDesc:
    "Penn Mobile Solutions develop professional web and mobile application development services.",
  SectionOneDescTwo:
    "A finished project isn’t the end. It’s the beginning of a better team building smarter technology from our unique consulting and training methodology.",
  SectionTwoHead: "Perfect Service Suite",
  SectionTwoDesc:
    "Penn Mobile Solutions is an IT consulting company, delivering complex world-class custom software solutions on various technology platforms for organizations across different verticals. As a software and product development company, we offer a full range of custom software development services, mobile application development for a wide variety of verticals and business domains.",
  SectionThreeHead: "Your idea, our deliverance",
  SectionThreeDesc:
    "We convert your brilliant ideas into alluring digital products that stand out from the rest of the crowd.",
  SectionFourHead: "Why choose us?",
  SectionFourDesc:
    "Your business depends on your IT systems working seamlessly. We’re confident that we are the only application development services and IT consulting business that provides these benefits to your business.",
  SectionFiveHead: "A Team You Can Trust",
  SectionFiveDesc:
    "Your go-to mobile app development company for innovative solutions and impeccable user experiences. We bring together cutting-edge technology and expert craftsmanship to bring your app ideas to life.",
  SectionSixHead: "Create an awesome mobile application Today with ",
  SectionSixHeadTwo: "Penn Mobile Solutions",
  SectionSizDesc:
    "Expertise is what defines us. We offer all type of custom mobile application development services and solutions for android and iOS platforms. We develop highly efficient and performance oriented mobile applications with the help of our highly skilled developers, specializing in different platforms. We offer you fully controlled and easy to manage apps with provide application monitoring services both computational and performance based.",
};

const SectionTwoHeadArr = [
  {
    id: 0,
    title: "Android Development",
    description:
      "Our designers will craft specific design for your android users. We’ll help you launch native Android application in google play store.",
    type: "android",
  },
  {
    id: 1,
    title: "iOS Development",
    description:
      "We build your iOS apps using Xcode, Swift, and Objective-C. We are in love with Swift and time and again move Objective-C code to Swift.",
    type: "ios",
  },
  {
    id: 2,
    title: "Consulting Solutions",
    description:
      "We provide customised IT Consulting, Project Management, IT Resourcing and on/off-shore Application Development.",
    type: "consult",
  },
  {
    id: 3,
    title: "Software Development",
    description:
      "We are an IT consultant company that specialize in providing Custom Software development and Web based Applications to a wide range of industries.",
    type: "software",
  },
];

const SectionFourHeadArr = [
  {
    id: 0,
    title: "Quality Assurance",
    description:
      "We build software that delivers results, and we have high standards for all the things we develop.",
  },
  {
    id: 1,
    title: "Cost efficiency",
    description:
      "Our affordable services will enable you to outrun your competitors without making a hole in your pocket.",
  },
  {
    id: 2,
    title: "Reliability",
    description:
      "Our emphasis on reliability and consistency has helped us establish long term business with our customers.",
  },
];

const homeSliderContentArr = [
  {
    id: 0,
    title: "Top talent",
    desc: "of experienced product managers, the smartest designers, and meticulous software engineers.",
  },
  {
    id: 1,
    title: "Rapidly turn",
    desc: "ideas into extraordinary products that drive business value.",
  },
  {
    id: 2,
    title: "Solid business",
    desc: "domain experience, technical expertise, profound knowledge of the latest industry trends.",
  },
];

export {
  homeData,
  SectionTwoHeadArr,
  SectionFourHeadArr,
  homeSliderContentArr,
};
